export { JsonPath } from './node_modules/sk-core/src/json-path.js';
export { JsonPathPlusDriver } from './node_modules/sk-core/src/impl/jsonpath/jsonpath-plus-driver.js';

export { XmlHttpClient } from './node_modules/sk-core/complets/http/xml-http-client.js';
export { SkConfig } from './node_modules/sk-core/src/sk-config.js';

export { AntdTheme } from './node_modules/sk-theme-antd/src/antd-theme.js';
export { JqueryTheme } from './node_modules/sk-theme-jquery/src/jquery-theme.js';
export { DefaultTheme } from './node_modules/sk-theme-default/src/default-theme.js';

export { SkElement } from './node_modules/sk-core/src/sk-element.js';
export { SkComponentImpl } from './node_modules/sk-core/src/impl/sk-component-impl.js';
export { CompRegistry } from './node_modules/sk-core/src/comp-registry.js';



export { LANGS_BY_CODES } from './node_modules/sk-core/src/sk-locale.js';
export { SkLocale } from './node_modules/sk-core/src/sk-locale.js';
export { SkLocaleEn } from './node_modules/sk-core/src/locale/sk-locale-en.js';
export { SkLocaleRu } from './node_modules/sk-core/src/locale/sk-locale-ru.js';
export { SkLocaleCn } from './node_modules/sk-core/src/locale/sk-locale-cn.js';

export { SkAlert } from './node_modules/sk-alert/src/sk-alert.js';
export { SkAlertImpl } from './node_modules/sk-alert/src/impl/sk-alert-impl.js';
export { AntdSkAlert } from './node_modules/sk-alert-antd/src/antd-sk-alert.js';
export { JquerySkAlert } from './node_modules/sk-alert-jquery/src/jquery-sk-alert.js';
export { DefaultSkAlert } from './node_modules/sk-alert-default/src/default-sk-alert.js';

export { SkButton } from './node_modules/sk-button/src/sk-button.js';
export { SkButtonImpl } from './node_modules/sk-button/src/impl/sk-button-impl.js';
export { AntdSkButton } from './node_modules/sk-button-antd/src/antd-sk-button.js';
export { JquerySkButton } from './node_modules/sk-button-jquery/src/jquery-sk-button.js';
export { DefaultSkButton } from './node_modules/sk-button-default/src/default-sk-button.js';

export { SkCheckbox } from './node_modules/sk-checkbox/src/sk-checkbox.js';
export { SkCheckboxImpl } from './node_modules/sk-checkbox/src/impl/sk-checkbox-impl.js';
export { AntdSkCheckbox } from './node_modules/sk-checkbox-antd/src/antd-sk-checkbox.js';
export { JquerySkCheckbox } from './node_modules/sk-checkbox-jquery/src/jquery-sk-checkbox.js';
export { DefaultSkCheckbox } from './node_modules/sk-checkbox-default/src/default-sk-checkbox.js';

export { SkDatePicker } from './node_modules/sk-datepicker/src/sk-datepicker.js';
export { SkDatepickerImpl } from './node_modules/sk-datepicker/src/impl/sk-datepicker-impl.js';
export { AntdSkDatepicker } from './node_modules/sk-datepicker-antd/src/antd-sk-datepicker.js';
export { JquerySkDatepicker } from './node_modules/sk-datepicker-jquery/src/jquery-sk-datepicker.js';
export { DefaultSkDatepicker } from './node_modules/sk-datepicker-default/src/default-sk-datepicker.js';

export { SkDialog } from './node_modules/sk-dialog/src/sk-dialog.js';
export { SkDialogImpl } from './node_modules/sk-dialog/src/impl/sk-dialog-impl.js';
export { AntdSkDialog } from './node_modules/sk-dialog-antd/src/antd-sk-dialog.js';
export { JquerySkDialog } from './node_modules/sk-dialog-jquery/src/jquery-sk-dialog.js';
export { DefaultSkDialog } from './node_modules/sk-dialog-default/src/default-sk-dialog.js';

export { SkTab } from './node_modules/sk-tabs/src/sk-tab.js';
export { AntdSkTab } from './node_modules/sk-tabs-antd/src/antd-sk-tab.js';
export { JquerySkTab } from './node_modules/sk-tabs-jquery/src/jquery-sk-tab.js';
export { DefaultSkTab } from './node_modules/sk-tabs-default/src/default-sk-tab.js';

export { SkTabs } from './node_modules/sk-tabs/src/sk-tabs.js';
export { SkTabsImpl } from './node_modules/sk-tabs/src/impl/sk-tabs-impl.js';
export { AntdSkTabs } from './node_modules/sk-tabs-antd/src/antd-sk-tabs.js';
export { JquerySkTabs } from './node_modules/sk-tabs-jquery/src/jquery-sk-tabs.js';
export { DefaultSkTabs } from './node_modules/sk-tabs-default/src/default-sk-tabs.js';

export { SkSltabs } from './node_modules/sk-tabs/src/sk-sltabs.js';
export { SkSltabsImpl } from './node_modules/sk-tabs/src/impl/sk-sltabs-impl.js';
export { AntdSkSltabs } from './node_modules/sk-tabs-antd/src/antd-sk-sltabs.js';
export { JquerySkSltabs } from './node_modules/sk-tabs-jquery/src/jquery-sk-sltabs.js';
export { DefaultSkSltabs } from './node_modules/sk-tabs-default/src/default-sk-sltabs.js';

export { SkAccordion } from './node_modules/sk-accordion/src/sk-accordion.js';
export { SkAccordionImpl } from './node_modules/sk-accordion/src/impl/sk-accordion-impl.js';
export { AntdSkAccordion } from './node_modules/sk-accordion-antd/src/antd-sk-accordion.js';
export { JquerySkAccordion } from './node_modules/sk-accordion-jquery/src/jquery-sk-accordion.js';
export { DefaultSkAccordion } from './node_modules/sk-accordion-default/src/default-sk-accordion.js';

export { SkForm } from './node_modules/sk-form/src/sk-form.js';
export { SkFormImpl } from './node_modules/sk-form/src/impl/sk-form-impl.js';
export { AntdSkForm } from './node_modules/sk-form-antd/src/antd-sk-form.js';
export { JquerySkForm } from './node_modules/sk-form-jquery/src/jquery-sk-form.js';
export { DefaultSkForm } from './node_modules/sk-form-default/src/default-sk-form.js';

export { SkSpinner } from './node_modules/sk-spinner/src/sk-spinner.js';
export { SkSpinnerImpl } from './node_modules/sk-spinner/src/impl/sk-spinner-impl.js';
export { AntdSkSpinner } from './node_modules/sk-spinner-antd/src/antd-sk-spinner.js';
export { JquerySkSpinner } from './node_modules/sk-spinner-jquery/src/jquery-sk-spinner.js';
export { DefaultSkSpinner } from './node_modules/sk-spinner-default/src/default-sk-spinner.js';

export { SkInput } from './node_modules/sk-input/src/sk-input.js';
export { SkInputImpl } from './node_modules/sk-input/src/impl/sk-input-impl.js';
export { AntdSkInput } from './node_modules/sk-input-antd/src/antd-sk-input.js';
export { JquerySkInput } from './node_modules/sk-input-jquery/src/jquery-sk-input.js';
export { DefaultSkInput } from './node_modules/sk-input-default/src/default-sk-input.js';

export { SkSelect } from './node_modules/sk-select/src/sk-select.js';
export { SkSelectImpl } from './node_modules/sk-select/src/impl/sk-select-impl.js';
export { AntdSkSelect } from './node_modules/sk-select-antd/src/antd-sk-select.js';
export { JquerySkSelect } from './node_modules/sk-select-jquery/src/jquery-sk-select.js';
export { DefaultSkSelect } from './node_modules/sk-select-default/src/default-sk-select.js';

export { SkSwitch } from './node_modules/sk-switch/src/sk-switch.js';
export { SkSwitchImpl } from './node_modules/sk-switch/src/impl/sk-switch-impl.js';
export { AntdSkSwitch } from './node_modules/sk-switch-antd/src/antd-sk-switch.js';
export { JquerySkSwitch } from './node_modules/sk-switch-jquery/src/jquery-sk-switch.js';
export { DefaultSkSwitch } from './node_modules/sk-switch-default/src/default-sk-switch.js';

export { SkTree } from './node_modules/sk-tree/src/sk-tree.js';
export { SkTreeImpl } from './node_modules/sk-tree/src/impl/sk-tree-impl.js';
export { AntdSkTree } from './node_modules/sk-tree-antd/src/antd-sk-tree.js';
export { JquerySkTree } from './node_modules/sk-tree-jquery/src/jquery-sk-tree.js';
export { DefaultSkTree } from './node_modules/sk-tree-default/src/default-sk-tree.js';

export { SkNavbar } from './node_modules/sk-navbar/src/sk-navbar.js';
export { SkNavbarImpl } from './node_modules/sk-navbar/src/impl/sk-navbar-impl.js';
export { AntdSkNavbar } from './node_modules/sk-navbar-antd/src/antd-sk-navbar.js';
export { JquerySkNavbar } from './node_modules/sk-navbar-jquery/src/jquery-sk-navbar.js';
export { DefaultSkNavbar } from './node_modules/sk-navbar-default/src/default-sk-navbar.js';

export { SkMenu } from './node_modules/sk-menu/src/sk-menu.js';
export { SkMenuImpl } from './node_modules/sk-menu/src/impl/sk-menu-impl.js';
export { AntdSkMenu } from './node_modules/sk-menu-antd/src/antd-sk-menu.js';
export { JquerySkMenu } from './node_modules/sk-menu-jquery/src/jquery-sk-menu.js';
export { DefaultSkMenu } from './node_modules/sk-menu-default/src/default-sk-menu.js';

export { SkToolbar } from './node_modules/sk-toolbar/src/sk-toolbar.js';
export { SkToolbarImpl } from './node_modules/sk-toolbar/src/impl/sk-toolbar-impl.js';
export { AntdSkToolbar } from './node_modules/sk-toolbar-antd/src/antd-sk-toolbar.js';
export { JquerySkToolbar } from './node_modules/sk-toolbar-jquery/src/jquery-sk-toolbar.js';
export { DefaultSkToolbar } from './node_modules/sk-toolbar-default/src/default-sk-toolbar.js';

export { SkApp } from './node_modules/sk-app/src/sk-app.js';
export { SkAppImpl } from './node_modules/sk-app/src/impl/sk-app-impl.js';
export { AntdSkApp } from './node_modules/sk-app-antd/src/antd-sk-app.js';
export { JquerySkApp } from './node_modules/sk-app-jquery/src/jquery-sk-app.js';
export { DefaultSkApp } from './node_modules/sk-app-default/src/default-sk-app.js';

export { SkRegistry } from './node_modules/sk-core/src/sk-registry.js';

import { skBootstrap, SK_ELEMENTS } from './src/sk-bootstrap.js';

function isAutoEnabled() {
    return (document &&
        (document.documentElement && document.documentElement.classList.contains('sk-auto'))
            || document.body && document.body.classList.contains('sk-auto'));
}

if (isAutoEnabled()) {
    skBootstrap(SK_ELEMENTS);
} else { // document was not yet fully loaded
    document.addEventListener("DOMContentLoaded", function(event) {
        if (isAutoEnabled()) {
            skBootstrap(SK_ELEMENTS);
        }
    });
}
