
import { SkConfig } from '../node_modules/sk-core/src/sk-config.js';
import { SkApp } from '../node_modules/sk-app/src/sk-app.js';
import { SkAlert } from '../node_modules/sk-alert/src/sk-alert.js';
import { SkButton } from '../node_modules/sk-button/src/sk-button.js';
import { SkCheckbox } from '../node_modules/sk-checkbox/src/sk-checkbox.js';
import { SkDatePicker } from '../node_modules/sk-datepicker/src/sk-datepicker.js';
import { SkDialog } from '../node_modules/sk-dialog/src/sk-dialog.js';
import { SkTab } from '../node_modules/sk-tabs/src/sk-tab.js';
import { SkTabs } from '../node_modules/sk-tabs/src/sk-tabs.js';
import { SkAccordion } from '../node_modules/sk-accordion/src/sk-accordion.js';
import { SkForm } from '../node_modules/sk-form/src/sk-form.js';
import { SkSpinner } from '../node_modules/sk-spinner/src/sk-spinner.js';
import { SkInput } from '../node_modules/sk-input/src/sk-input.js';
import { SkSelect } from '../node_modules/sk-select/src/sk-select.js';
import { SkSwitch } from '../node_modules/sk-switch/src/sk-switch.js';
import { SkTree } from '../node_modules/sk-tree/src/sk-tree.js';
import { SkNavbar } from '../node_modules/sk-navbar/src/sk-navbar.js';
import { SkRegistry } from '../node_modules/sk-core/src/sk-registry.js';

export var SK_ELEMENTS = {
    'sk-config': SkConfig,
    'sk-app': SkApp,
    'sk-alert': SkAlert,
    'sk-button': SkButton,
    'sk-datepicker': SkDatePicker,
    'sk-checkbox': SkCheckbox,
    'sk-dialog': SkDialog,
    'sk-tabs': SkTabs,
    'sk-accordion': SkAccordion,
    'sk-form': SkForm,
    'sk-spinner': SkSpinner,
    'sk-input': SkInput,
    'sk-select': SkSelect,
    'sk-switch': SkSwitch,
    'sk-tree': SkTree,
    'sk-navbar': SkNavbar,
    'sk-tab': SkTab,
    'sk-registry': SkRegistry
};

export function skRegisterElements(elements) {
    let ce = window.customElements;
    for (let tag of Object.keys(elements)) {
        if (!ce.get(tag)) {
            ce.define(tag, elements[tag]);
        }
    }
}

export function skPreloadTemplates(theme) {
    return fetch('/node_modules/skinny-widgets/dist/' + theme + '.templates.html')
        .then(response => response.text())
        .then(text => {
            document.body.insertAdjacentHTML('beforeend', text);
        });
}

export async function skBootstrap(elements) {
    console.debug('found autoload enabling triggers, start bootstrapping sk components');
    console.debug('end bootstrapping sk componnents');
    let themeMatch = (document.documentElement && document.documentElement.className.match(/sk-theme-(\w+)/))
        || (document.body && document.body.className.match(/sk-theme-(\w+)/));
    if (themeMatch !== null) {
        let theme = themeMatch[1];
        let preLoadTemplates = (! (document.documentElement && document.documentElement.classList.contains('sk-auto-notpl'))
            && ! (document.body && document.body.classList.contains('sk-auto-notpl'))) && theme;
        if (theme && preLoadTemplates) {
            console.debug('preloading theme', theme);
            let tplLoad = await skPreloadTemplates(theme);
            skRegisterElements(elements);
        } else {
            skRegisterElements(elements);
        }
    } else {
        skRegisterElements(elements);
    }
}