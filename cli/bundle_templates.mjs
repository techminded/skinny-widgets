

import { bundleTemplates } from "sk-cl-tools";


bundleTemplates('jquery-templates.json', "sk-jquery");
bundleTemplates('antd-templates.json', "sk-antd");
bundleTemplates('default-templates.json', "sk-default");
