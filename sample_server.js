var fs = require('fs');

var path = require('path');
var express = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');
var filePath = path.join(__dirname, 'sample_response.json');


app.options('*', cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

function sleep(ms) {
    ms += new Date().getTime();
    while (new Date() < ms){}
}

function parseBody(body) {
    let data = {};
    for (let token of Object.keys(body)) {
        let allFields = body[token].split('Content-Disposition: form-data;');
        for (let field of allFields) {
            let fieldData = field.split('\r\n');
            let name = fieldData[0].match(/"(.*)"/)[1];
            let value = fieldData[2];
            data[name] = value;
        }
    }
    return data;
}

app.post('/submit', cors(), function (req, res, next) {
    //sleep(1000 * 15);
    //res.contentType('application/json');
    //console.log('Got body:', req.body);
    console.log(parseBody(req.body));

    var readable = fs.createReadStream(filePath);
    readable.pipe(res);
});

app.use(express.static('.'));

app.listen(8080, function () {
    console.log('CORS-enabled web server listening on port 8080')
});