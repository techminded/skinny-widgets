
import babel from '@rollup/plugin-babel';
import nodeResolve from '@rollup/plugin-node-resolve';
import commonJS from '@rollup/plugin-commonjs';

export default {
  input: 'index.js',
  output: {
    globals: {
    },
    file: 'dist/skinny-widgets-bundle.js',
    format: 'umd',
    name: 'window',
    extend: true,
    sourcemap: true,
  },
  external: [  ],
  plugins: [
    babel({ babelHelpers: 'inline' }),
    nodeResolve({ mainFields: ['jsnext:main']}),
    commonJS({})
  ]
};
